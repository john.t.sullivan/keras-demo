import numpy as np

from keras.utils import np_utils
from keras.datasets import mnist

data_dim = 784
label_dim = 10

batch_size = 32
nb_epoch = 10

reshape_X = lambda x: x.reshape([-1, data_dim]).astype('float32') / 255.0
reshape_y = lambda y: np_utils.to_categorical(y, label_dim)

print 'loading'
(X_train, y_train), (X_test, y_test) = mnist.load_data()

print 'loaded'

X_train = reshape_X(X_train)
X_test = reshape_X(X_test)

print 'reshaped x'

y_train = reshape_y(y_train)
y_test = reshape_y(y_test)

print 'reshaped y'

from keras.models import Sequential
from keras.layers import Dense, Activation

model = Sequential()

model.add(Dense(output_dim=batch_size, input_dim=data_dim))
model.add(Activation('relu'))
model.add(Dense(output_dim=label_dim))
model.add(Activation('softmax'))

model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

model.summary()

model.fit(X_train, y_train, batch_size=batch_size, nb_epoch=nb_epoch, verbose=1, validation_data=(X_test, y_test))
score = model.evaluate(X_test, y_test, verbose=1)
print 'Test score:', score[0]
print 'Test accuracy:', score[1]
